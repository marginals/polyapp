package com.deledzis.polyapp.classes

enum class Degree(val title: String) {
    NO_DEGREE(title = "Нет степени"),
    ASSISTANT(title = "Доцент"),
    CANDIDATE(title = "Кандидат наук"),
    DOCTOR(title = "Доктор наук");

    override fun toString() = this.title
}