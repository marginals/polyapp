package com.deledzis.polyapp.classes.extensions

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.content.Context.WINDOW_SERVICE
import android.graphics.Point
import android.view.View
import android.view.WindowManager
import androidx.annotation.ColorRes
import androidx.core.view.ViewCompat
import com.deledzis.polyapp.R
import io.reactivex.Completable
import io.reactivex.subjects.CompletableSubject

fun View.show(): View {
    this.visibility = View.VISIBLE
    return this
}

fun View.hide(): View {
    this.visibility = View.GONE
    return this
}

fun View.getViewHeight(): Float {
    val wm = this.context.getSystemService(WINDOW_SERVICE) as WindowManager
    val display = wm.defaultDisplay

    val deviceWidth: Int

    val size = Point()
    display.getSize(size)
    deviceWidth = size.x

    val widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST)
    val heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
    this.measure(widthMeasureSpec, heightMeasureSpec)

    return this.measuredHeight.toFloat()
}

fun View.fadeIn(duration: Long = 200): Completable {
    val animationSubject = CompletableSubject.create()
    return animationSubject.doOnSubscribe {
        ViewCompat.animate(this)
            .setDuration(duration)
            .alpha(1.0f)
            .withEndAction {
                animationSubject.onComplete()
            }
    }
}

fun View.fadeOut(duration: Long = 200): Completable {
    val animationSubject = CompletableSubject.create()
    return animationSubject.doOnSubscribe {
        ViewCompat.animate(this)
            .setDuration(duration)
            .alpha(0.0f)
            .withEndAction {
                animationSubject.onComplete()
            }
    }
}

fun View.fadeInElevation(from: Float, to: Float, duration: Long = 200): Completable {
    val animationSubject = CompletableSubject.create()
    return animationSubject.doOnSubscribe {
        val animator = ValueAnimator.ofFloat(from, to)
        animator.duration = duration
        animator.start()

        animator.addUpdateListener { animation ->
            val animatedValue = animation.animatedValue as Float
            this.elevation = animatedValue
            if (animatedValue >= elevation) animationSubject.onComplete()
        }
    }
}

fun View.fadeOutElevation(from: Float, to: Float, duration: Long = 200): Completable {
    val animationSubject = CompletableSubject.create()
    return animationSubject.doOnSubscribe {
        val animator = ValueAnimator.ofFloat(from, to)
        animator.duration = duration
        animator.start()

        animator.addUpdateListener { animation ->
            val animatedValue = animation.animatedValue as Float
            this.elevation = animatedValue
            if (animatedValue == 0.0f) animationSubject.onComplete()
        }
    }
}

fun View.fadeInColor(@ColorRes color: Int, duration: Long = 200): Completable {
    val animationSubject = CompletableSubject.create()
    return animationSubject.doOnSubscribe {
        val colorFrom = context.colorFrom(R.color.transparent)
        val colorTo = context.colorFrom(color)
        val animator = ValueAnimator.ofObject(ArgbEvaluator(), colorFrom, colorTo)
        animator.duration = duration
        animator.start()

        animator.addUpdateListener { animation ->
            val animatedValue = animation.animatedValue as Int
            this.setBackgroundColor(animatedValue)
            if (animatedValue == colorTo) animationSubject.onComplete()
        }
    }
}

fun View.fadeOutColor(@ColorRes color: Int, duration: Long = 200): Completable {
    val animationSubject = CompletableSubject.create()
    return animationSubject.doOnSubscribe {
        val colorFrom = context.colorFrom(color)
        val colorTo = context.colorFrom(R.color.transparent)
        val animator = ValueAnimator.ofObject(ArgbEvaluator(), colorFrom, colorTo)
        animator.duration = duration
        animator.start()

        animator.addUpdateListener { animation ->
            val animatedValue = animation.animatedValue as Int
            this.setBackgroundColor(animatedValue)
            if (animatedValue == colorTo) animationSubject.onComplete()
        }
    }
}