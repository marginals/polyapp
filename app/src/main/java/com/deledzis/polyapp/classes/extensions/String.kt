package com.deledzis.polyapp.classes.extensions

import java.util.*
import java.util.regex.Pattern

fun String.toColor(): Int = toInt(16) or (0xff shl 24)

fun String.toFullDate(): Date = FULL_DATE_FORMATTER.parse(this) ?: Date(0)

fun String.isEmail(): Boolean = Pattern.compile(
    "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
            ")+"
).matcher(this).matches()

fun String.isNotEmail(): Boolean = !isEmail()