package com.deledzis.polyapp.classes.extensions

import android.animation.ValueAnimator
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView
import io.reactivex.Completable
import io.reactivex.subjects.CompletableSubject

fun ObservableScrollView.smoothScrollVerticallyTo(y: Int, duration: Long = 200): Completable {
    val animationSubject = CompletableSubject.create()
    return animationSubject.doOnSubscribe {
        val animator = ValueAnimator.ofInt(this.scrollY, y)
        animator.duration = duration
        animator.start()

        animator.addUpdateListener { animation ->
            val animatedValue = animation.animatedValue as Int
            this.scrollTo(0, animatedValue)
            if (animatedValue == y) animationSubject.onComplete()
        }
    }
}