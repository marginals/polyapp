package com.deledzis.polyapp.classes.extensions

import android.util.Log
import com.deledzis.polyapp.util.isDebug

/** Lazy wrapper over [Log.v] */
inline fun <reified T> T.logv(
    onlyInDebugMode: Boolean = true,
    lazyMessage: () -> String
) {
    log(onlyInDebugMode) {
        Log.v(
            getClassSimpleName<T>(),
            lazyMessage.invoke()
        )
    }
}

/** Lazy wrapper over [Log.d] */
inline fun <reified T> T.logd(
    onlyInDebugMode: Boolean = true,
    lazyMessage: () -> String
) {
    log(onlyInDebugMode) {
        Log.d(
            getClassSimpleName<T>(),
            lazyMessage.invoke()
        )
    }
}

/** Lazy wrapper over [Log.i] */
inline fun <reified T> T.logi(
    onlyInDebugMode: Boolean = true,
    lazyMessage: () -> String
) {
    log(onlyInDebugMode) {
        Log.i(
            getClassSimpleName<T>(),
            lazyMessage.invoke()
        )
    }
}

/** Lazy wrapper over [Log.i] */
inline fun <reified T> T.logw(
    onlyInDebugMode: Boolean = true,
    lazyMessage: () -> String
) {
    log(onlyInDebugMode) {
        Log.w(
            getClassSimpleName<T>(),
            lazyMessage.invoke()
        )
    }
}

/** Lazy wrapper over [Log.e] */
inline fun <reified T> T.loge(
    onlyInDebugMode: Boolean = true,
    exception: Exception? = null,
    lazyMessage: () -> String
) {
    log(onlyInDebugMode) {
        Log.e(
            getClassSimpleName<T>(),
            lazyMessage.invoke(),
            exception
        )
    }
}

inline fun log(onlyInDebugMode: Boolean, logger: () -> Unit) {
    when {
        onlyInDebugMode && isDebug -> logger()
        !onlyInDebugMode -> logger()
    }
}

/**
 * Utility that returns the name of the class from within it is invoked.
 */
inline fun <reified T> getClassSimpleName(): String =
    if (T::class.java.simpleName.isNotBlank()) {
        T::class.java.simpleName
    } else {
        "Anonymous"
    }