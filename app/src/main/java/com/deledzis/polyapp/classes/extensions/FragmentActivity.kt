package com.deledzis.polyapp.classes.extensions

import android.os.Build
import android.view.View
import android.view.WindowManager
import androidx.annotation.ColorRes
import androidx.fragment.app.FragmentActivity
import com.deledzis.polyapp.R

fun FragmentActivity.styleStatusBarIcons(@ColorRes statusBarColor: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = colorFrom(statusBarColor)
    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val decor = window.decorView
        if (statusBarColor == R.color.white) {
            // To change status bar icons tint color mode to dark
            decor.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        } else {
            // To change status bar icons tint color mode to white.
            decor.systemUiVisibility = 0
        }
    }
}