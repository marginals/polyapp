package com.deledzis.polyapp.classes.extensions

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.TypedValue
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes

fun Context.drawableCompatFrom(@DrawableRes drawable: Int): Drawable {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        this.resources.getDrawable(drawable, this.theme)
    } else {
        @Suppress("DEPRECATION")
        this.resources.getDrawable(drawable)
    }
}

fun Context.colorFrom(@ColorRes color: Int): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        this.resources.getColor(color, this.theme)
    } else {
        @Suppress("DEPRECATION")
        this.resources.getColor(color)
    }
}

fun Context.colorStateListFrom(@ColorRes color: Int): ColorStateList {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        this.resources.getColorStateList(color, this.theme)
    } else {
        @Suppress("DEPRECATION")
        this.resources.getColorStateList(color)
    }
}

fun Context.getDp(value: Float) = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_DIP,
    value,
    resources.displayMetrics
)