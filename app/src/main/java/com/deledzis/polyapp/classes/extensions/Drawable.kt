package com.deledzis.polyapp.classes.extensions

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable
import io.reactivex.Completable
import io.reactivex.subjects.CompletableSubject

fun Drawable.withColorFilter(color: Int): Drawable {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        this.colorFilter = BlendModeColorFilter(color, BlendMode.SRC_ATOP)
    } else {
        @Suppress("DEPRECATION")
        this.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
    }

    return this
}

fun DrawerArrowDrawable.changeColor(to: Int, duration: Long = 200): Completable {
    val animationSubject = CompletableSubject.create()
    return animationSubject.doOnSubscribe {
        val animator = ValueAnimator.ofObject(ArgbEvaluator(), this.color, to)
        animator.duration = duration
        animator.start()

        animator.addUpdateListener { animation ->
            val animatedValue = animation.animatedValue as Int
            this.color = animatedValue
            if (animatedValue == to) animationSubject.onComplete()
        }
    }
}