package com.deledzis.polyapp.classes.extensions

import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.util.TypedValue
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.fragment.app.Fragment

fun Fragment.drawableCompatFrom(@DrawableRes drawable: Int): Drawable {
    return requireContext().drawableCompatFrom(drawable)
}

fun Fragment.colorFrom(@ColorRes color: Int): Int {
    return requireContext().colorFrom(color)
}

fun Fragment.colorStateListFrom(@ColorRes color: Int): ColorStateList {
    return requireContext().colorStateListFrom(color)
}

fun Fragment.getDp(value: Float) = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_DIP,
    value,
    resources.displayMetrics
)

fun Fragment.styleStatusBarIcons(@ColorRes statusBarColor: Int) {
    requireActivity().styleStatusBarIcons(statusBarColor)
}