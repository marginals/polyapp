package com.deledzis.polyapp.classes.extensions

import java.text.SimpleDateFormat
import java.util.*

val FULL_DATE_FORMATTER = SimpleDateFormat(
    "yyyy-MM-dd HH:mm:ss",
    Locale.forLanguageTag("ru-RU")
)

fun Date.toFullDateString(): String = FULL_DATE_FORMATTER.format(this)

fun Date.isSameDay(day: Date): Boolean {
    val cal0 = Calendar.getInstance()
    cal0.time = this

    val cal1 = Calendar.getInstance()
    cal1.time = day

    return cal0.get(Calendar.YEAR) == cal1.get(Calendar.YEAR)
            && cal0.get(Calendar.MONTH) == cal1.get(Calendar.MONTH)
            && cal0.get(Calendar.DAY_OF_MONTH) == cal1.get(Calendar.DAY_OF_MONTH)
}

fun Date.isToday(): Boolean = isSameDay(Date(System.currentTimeMillis()))