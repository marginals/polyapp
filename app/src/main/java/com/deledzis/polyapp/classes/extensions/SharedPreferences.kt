package com.deledzis.polyapp.classes.extensions

import android.content.SharedPreferences

fun SharedPreferences.setInt(key: String, value: Int) {
    edit().putInt(key, value).apply()
}

fun SharedPreferences.setLong(key: String, value: Long) {
    edit().putLong(key, value).apply()
}

fun SharedPreferences.setBool(key: String, value: Boolean) {
    edit().putBoolean(key, value).apply()
}

fun SharedPreferences.setFloat(key: String, value: Float) {
    edit().putFloat(key, value).apply()
}

fun SharedPreferences.setDouble(key: String, value: Double) {
    edit().putFloat(key, value.toFloat()).apply()
}

fun SharedPreferences.setString(key: String, value: String) {
    edit().putString(key, value).apply()
}

fun SharedPreferences.getDouble(key: String, def: Double = 0.0): Double =
    getFloat(key, def.toFloat()).toDouble()
