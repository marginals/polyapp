package com.deledzis.polyapp.ui.timeline

import androidx.annotation.LayoutRes
import com.deledzis.polyapp.R
import com.deledzis.polyapp.databinding.FragmentTimelineBinding
import com.deledzis.polyapp.ui.base.BaseFragmentWithObservable
import com.deledzis.polyapp.ui.base.getAndroidViewModel

class TimelineFragment : BaseFragmentWithObservable<TimelineViewModel, FragmentTimelineBinding>() {

    @LayoutRes
    override val layoutId: Int = R.layout.fragment_timeline

    override fun createViewModel(): TimelineViewModel = getAndroidViewModel()

    override fun onBindData() {
        super.onBindData()
        dataBinding.viewModel = viewModel
    }

    override fun onInjectDependencies() {
        commonInjector.inject(viewModel)
    }
}
