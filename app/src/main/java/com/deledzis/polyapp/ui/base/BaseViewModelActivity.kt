package com.deledzis.polyapp.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.deledzis.polyapp.App
import com.deledzis.polyapp.di.components.CommonComponent

abstract class BaseViewModelActivity<VM : BaseViewModel, VDB : ViewDataBinding> :
    AppCompatActivity(),
    BaseViewModelView<VM, VDB> {

    override lateinit var dataBinding: VDB
    override val viewModel by lazy { createViewModel() }
    protected val commonInjector: CommonComponent
        get() = (application as App).commonInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataBinding = DataBindingUtil.inflate(
            LayoutInflater.from(this),
            layoutId,
            null,
            false
        )
        dataBinding.lifecycleOwner = this
        setContentView(dataBinding.root)

        onInjectDependencies()
    }
}
