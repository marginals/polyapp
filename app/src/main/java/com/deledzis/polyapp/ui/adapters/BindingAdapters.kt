package com.deledzis.polyapp.ui.adapters

import android.view.View
import android.widget.LinearLayout
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import com.deledzis.polyapp.R
import com.deledzis.polyapp.classes.extensions.getDp

object BindingAdapters {

    @JvmStatic
    @BindingAdapter("app:margins")
    fun setTopMarginFromBoolean(view: View, isNestedFragment: MutableLiveData<Boolean>) {
        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        params.topMargin = if (isNestedFragment.value == true) {
             view.context.getDp(48.0f).toInt()
        } else {
            view.context.getDp(16.0f).toInt()
        }
        params.marginStart = view.resources.getDimension(R.dimen.default_margin).toInt()
        params.marginEnd = view.resources.getDimension(R.dimen.default_margin).toInt()
        view.layoutParams = params
    }
}