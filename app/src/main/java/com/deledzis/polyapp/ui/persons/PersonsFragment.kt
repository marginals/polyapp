package com.deledzis.polyapp.ui.persons

import androidx.annotation.LayoutRes
import com.deledzis.polyapp.R
import com.deledzis.polyapp.databinding.FragmentPersonsBinding
import com.deledzis.polyapp.ui.base.BaseFragmentWithObservable
import com.deledzis.polyapp.ui.base.getAndroidViewModel

class PersonsFragment : BaseFragmentWithObservable<PersonsViewModel, FragmentPersonsBinding>() {

    @LayoutRes
    override val layoutId: Int = R.layout.fragment_persons

    override fun createViewModel(): PersonsViewModel = getAndroidViewModel()

    override fun onBindData() {
        super.onBindData()
        dataBinding.viewModel = viewModel
    }

    override fun onInjectDependencies() {
        commonInjector.inject(viewModel)
    }
}