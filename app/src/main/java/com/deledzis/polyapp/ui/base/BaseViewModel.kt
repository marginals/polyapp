package com.deledzis.polyapp.ui.base

import android.app.Application
import android.os.Bundle
import android.view.View
import androidx.annotation.IdRes
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import com.deledzis.polyapp.R

open class BaseViewModel(app: Application) : AndroidViewModel(app) {

    private var _nestedFragment = MutableLiveData(false)
    val nestedFragment = _nestedFragment

    fun setAsNested() {
        _nestedFragment.value = true
    }

    fun setAsTop() {
        _nestedFragment.value = false
    }

    private fun getDefaultNavOptions() = NavOptions.Builder()
        .setEnterAnim(R.anim.slide_in_left)
        .setExitAnim(R.anim.slide_out_left)
        .setPopEnterAnim(R.anim.slide_in_right)
        .setPopExitAnim(R.anim.slide_out_right)
        .build()

    fun navigateOnClick(view: View, @IdRes actionId: Int, bundle: Bundle? = null) {
        view.findNavController().navigate(actionId, bundle, getDefaultNavOptions())
    }

    fun navigateToLocations(view: View, bundle: Bundle = Bundle.EMPTY) {
        navigateOnClick(view, R.id.action_global_fragment_locations, bundle)
    }
}