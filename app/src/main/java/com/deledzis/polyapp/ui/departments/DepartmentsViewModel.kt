package com.deledzis.polyapp.ui.departments

import android.app.Application
import android.os.Bundle
import android.view.View
import com.deledzis.polyapp.R
import com.deledzis.polyapp.ui.base.BaseViewModel

class DepartmentsViewModel(app: Application) : BaseViewModel(app) {
    fun openDepartment(view: View) {
        // TODO: replace with real
        val bundle = Bundle.EMPTY
        navigateOnClick(view, R.id.action_departments_fragment_to_department_fragment, bundle)
    }
}