package com.deledzis.polyapp.ui.studyprograms

import android.app.Application
import android.os.Bundle
import android.view.View
import com.deledzis.polyapp.R
import com.deledzis.polyapp.ui.base.BaseViewModel

class StudyProgramsViewModel(app: Application) : BaseViewModel(app) {

    fun openStudyProgram(view: View) {
        // TODO: replace with real
        val bundle = Bundle.EMPTY
        navigateOnClick(view, R.id.action_study_programs_fragment_to_study_program_fragment, bundle)
    }
}