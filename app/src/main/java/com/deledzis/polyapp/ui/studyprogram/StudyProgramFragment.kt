package com.deledzis.polyapp.ui.studyprogram

import androidx.annotation.LayoutRes
import com.deledzis.polyapp.R
import com.deledzis.polyapp.databinding.FragmentStudyProgramBinding
import com.deledzis.polyapp.ui.base.BaseFragmentWithObservable
import com.deledzis.polyapp.ui.base.getAndroidViewModel

class StudyProgramFragment : BaseFragmentWithObservable<StudyProgramViewModel, FragmentStudyProgramBinding>() {

    @LayoutRes
    override val layoutId: Int = R.layout.fragment_study_program

    override fun createViewModel(): StudyProgramViewModel = getAndroidViewModel()

    override fun onBindData() {
        super.onBindData()
        dataBinding.viewModel = viewModel
    }

    override fun onInjectDependencies() {
        commonInjector.inject(viewModel)
    }
}