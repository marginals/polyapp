package com.deledzis.polyapp.ui.base.host

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.Toolbar
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.deledzis.polyapp.R
import com.deledzis.polyapp.databinding.ActivityHostBinding
import com.deledzis.polyapp.ui.base.BaseViewModel
import com.deledzis.polyapp.ui.base.BaseViewModelActivity
import com.deledzis.polyapp.ui.base.getAndroidViewModel
import kotlinx.android.synthetic.main.layout_appbar.view.*

open class HostActivity : BaseViewModelActivity<BaseViewModel, ActivityHostBinding>() {

    @LayoutRes
    override val layoutId: Int = R.layout.activity_host

    override fun createViewModel(): BaseViewModel = getAndroidViewModel()

    val toolbar: Toolbar
        get() = dataBinding.actionBar.toolbar

    val reveal: View
        get() = dataBinding.actionBar.reveal

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // TODO: Handle deep links
        //  when opened from deep link, Action = VIEW, data contains requested URL
        /*val action: String? = intent?.action
        val data: Uri? = intent?.data*/
        setSupportActionBar(dataBinding.actionBar.toolbar)
        dataBinding.actionBar.toolbar.setupWithNavController(provideNavController(), provideAppBarConfiguration())

        setupBottomNavigation()
    }

    override fun onInjectDependencies() {
        commonInjector.inject(viewModel)
    }

    private fun setupBottomNavigation() {
        dataBinding.bottomNavigationView.setupWithNavController(
            Navigation.findNavController(
                this,
                R.id.navigation_host_fragment
            )
        )
        dataBinding.bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            NavigationUI.onNavDestinationSelected(
                item,
                Navigation.findNavController(this, R.id.navigation_host_fragment)
            )
        }
    }

    private fun provideNavController(): NavController {
        return Navigation.findNavController(this, R.id.navigation_host_fragment)
    }

    private fun provideAppBarConfiguration(): AppBarConfiguration {
        return AppBarConfiguration(
            setOf(
                R.id.fragment_timeline,
                R.id.fragment_institutes,
                R.id.fragment_locations,
                R.id.fragment_faq
            )
        )
    }

    fun selectStructureMenuItem() {
        dataBinding.bottomNavigationView.menu.findItem(R.id.fragment_institutes).isChecked = true
    }

    fun showBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }
}
