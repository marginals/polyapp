package com.deledzis.polyapp.ui.person

import android.app.Application
import android.os.Bundle
import android.view.View
import com.deledzis.polyapp.ui.base.BaseViewModel

class PersonViewModel(app: Application) : BaseViewModel(app) {

    fun openLocations(view: View) {
        val bundle = Bundle().apply { putInt("location_id", 7) }
        navigateToLocations(view, bundle)
    }
}