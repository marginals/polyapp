package com.deledzis.polyapp.ui.faq

import androidx.annotation.LayoutRes
import com.deledzis.polyapp.R
import com.deledzis.polyapp.databinding.FragmentFaqBinding
import com.deledzis.polyapp.ui.base.BaseFragmentWithObservable
import com.deledzis.polyapp.ui.base.getAndroidViewModel

class FaqFragment : BaseFragmentWithObservable<FaqViewModel, FragmentFaqBinding>() {

    @LayoutRes
    override val layoutId: Int = R.layout.fragment_faq

    override fun createViewModel(): FaqViewModel = getAndroidViewModel()

    override fun onBindData() {
        super.onBindData()
        dataBinding.viewModel = viewModel
    }

    override fun onInjectDependencies() {
        commonInjector.inject(viewModel)
    }
}
