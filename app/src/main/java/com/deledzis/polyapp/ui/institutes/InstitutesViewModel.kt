package com.deledzis.polyapp.ui.institutes

import android.app.Application
import android.os.Bundle
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.deledzis.polyapp.R
import com.deledzis.polyapp.ui.base.BaseViewModel

class InstitutesViewModel(val app: Application) : BaseViewModel(app) {
    fun openInstitute(view: View) {
        // TODO: replace with real
        val bundle = Bundle.EMPTY
        navigateOnClick(view, R.id.action_institutes_fragment_to_institute_fragment, bundle)
    }
}