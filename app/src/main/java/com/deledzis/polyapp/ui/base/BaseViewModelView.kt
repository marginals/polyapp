package com.deledzis.polyapp.ui.base

import androidx.annotation.ColorRes
import androidx.databinding.ViewDataBinding
import com.deledzis.polyapp.R

interface BaseViewModelView<VM : BaseViewModel, VDB : ViewDataBinding> {
    var dataBinding: VDB
    val layoutId: Int
    val viewModel: VM
    val statusBarColorTint: Int
        @ColorRes get() = R.color.white

    fun createViewModel(): VM
    fun onBindData() {}
    fun onInjectDependencies() {}
}