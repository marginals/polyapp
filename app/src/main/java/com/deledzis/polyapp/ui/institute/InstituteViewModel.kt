package com.deledzis.polyapp.ui.institute

import android.app.Application
import android.os.Bundle
import android.view.View
import com.deledzis.polyapp.R
import com.deledzis.polyapp.ui.base.BaseViewModel

class InstituteViewModel(app: Application) : BaseViewModel(app) {

    fun openStudyPrograms(view: View) {
        // TODO: replace with real
        val bundle = Bundle.EMPTY
        navigateOnClick(view, R.id.action_institute_fragment_to_study_programs_fragment, bundle)
    }

    fun openDepartments(view: View) {
        // TODO: replace with real
        val bundle = Bundle.EMPTY
        navigateOnClick(view, R.id.action_institute_fragment_to_departments_fragment, bundle)
    }

    fun openDirectorate(view: View) {
        // TODO: replace with real
        val bundle = Bundle.EMPTY
        navigateOnClick(view, R.id.action_institute_fragment_to_persons_fragment, bundle)
    }
}