package com.deledzis.polyapp.ui.institute

import androidx.annotation.LayoutRes
import com.deledzis.polyapp.R
import com.deledzis.polyapp.databinding.FragmentInstituteBinding
import com.deledzis.polyapp.ui.base.BaseFragmentWithObservable
import com.deledzis.polyapp.ui.base.getAndroidViewModel

class InstituteFragment : BaseFragmentWithObservable<InstituteViewModel, FragmentInstituteBinding>() {

    @LayoutRes
    override val layoutId: Int = R.layout.fragment_institute

    override fun createViewModel(): InstituteViewModel = getAndroidViewModel()

    override fun onBindData() {
        super.onBindData()
        dataBinding.viewModel = viewModel
    }

    override fun onInjectDependencies() {
        commonInjector.inject(viewModel)
    }
}