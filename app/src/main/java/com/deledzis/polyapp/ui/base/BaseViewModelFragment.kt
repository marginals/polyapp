package com.deledzis.polyapp.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.deledzis.polyapp.App
import com.deledzis.polyapp.R
import com.deledzis.polyapp.classes.extensions.styleStatusBarIcons
import com.deledzis.polyapp.di.components.CommonComponent
import com.deledzis.polyapp.ui.base.host.HostActivity

abstract class BaseViewModelFragment<VM : BaseViewModel, VDB : ViewDataBinding> : Fragment(),
    BaseViewModelView<VM, VDB> {

    protected lateinit var activity: HostActivity
    override lateinit var dataBinding: VDB
    override val viewModel by lazy { createViewModel() }
    protected val commonInjector: CommonComponent
        get() = (requireActivity().application as App).commonInjector

    override val statusBarColorTint: Int = R.color.poly_green_dark

    protected var toolbarTitle: TextView? = null
    protected var toolbarIcon: DrawerArrowDrawable? = null
    protected var reveal: View? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as HostActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(
            inflater,
            layoutId,
            container,
            false
        )
        dataBinding.lifecycleOwner = this
        toolbarTitle = activity.toolbar.getChildAt(0) as TextView
        activity.toolbar.navigationIcon?.let {
            toolbarIcon = it as DrawerArrowDrawable
            viewModel.setAsNested()
        } ?: viewModel.setAsTop()
        reveal = activity.reveal

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onBindData()
        onInjectDependencies()
        styleStatusBarIcons(statusBarColorTint)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        toolbarTitle = null
        toolbarIcon = null
        reveal = null
    }
}