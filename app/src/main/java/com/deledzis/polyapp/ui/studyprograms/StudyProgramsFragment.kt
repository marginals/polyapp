package com.deledzis.polyapp.ui.studyprograms

import androidx.annotation.LayoutRes
import com.deledzis.polyapp.R
import com.deledzis.polyapp.databinding.FragmentStudyProgramsBinding
import com.deledzis.polyapp.ui.base.BaseFragmentWithObservable
import com.deledzis.polyapp.ui.base.getAndroidViewModel

class StudyProgramsFragment : BaseFragmentWithObservable<StudyProgramsViewModel, FragmentStudyProgramsBinding>() {

    @LayoutRes
    override val layoutId: Int = R.layout.fragment_study_programs

    override fun createViewModel(): StudyProgramsViewModel = getAndroidViewModel()

    override fun onBindData() {
        super.onBindData()
        dataBinding.viewModel = viewModel
    }

    override fun onInjectDependencies() {
        commonInjector.inject(viewModel)
    }
}