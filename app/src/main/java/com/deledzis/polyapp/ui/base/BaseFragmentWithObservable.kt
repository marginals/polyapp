package com.deledzis.polyapp.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.deledzis.polyapp.R
import com.deledzis.polyapp.classes.extensions.*
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks
import com.github.ksoichiro.android.observablescrollview.ScrollState
import com.google.android.material.textview.MaterialTextView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

abstract class BaseFragmentWithObservable<VM : BaseViewModel, VDB : ViewDataBinding> :
    BaseViewModelFragment<VM, VDB>() {

    private var fragmentTitleText: MaterialTextView? = null
    private var observableScrollView: ObservableScrollView? = null

    private val titleHeight: Float by lazy {
        maxOf(
            fragmentTitleText?.getViewHeight() ?: 0.0f,
            activity.toolbar.getViewHeight()
        )
    }
    private val toolbarElevation: Float by lazy { getDp(8.0f) }

    private var lastScrollState: ScrollState = ScrollState.STOP
    private var scrolling = false
    private var animating = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        fragmentTitleText = dataBinding.root.findViewById(R.id.fragment_title)
        observableScrollView = dataBinding.root.findViewById(R.id.observable_scroll_view)

        return dataBinding.root
    }

    override fun onBindData() {
        super.onBindData()

        resetViews()
        bindObservable()
    }

    override fun onDestroy() {
        super.onDestroy()
        fragmentTitleText = null
        observableScrollView = null
    }

    private fun resetViews() {
        toolbarTitle?.alpha = 0.0f
        toolbarTitle?.setTextColor(colorFrom(R.color.white))
        toolbarIcon?.color = colorFrom(R.color.dark)
        reveal?.alpha = 0.0f
        reveal?.backgroundTintList = colorStateListFrom(R.color.poly_green)
    }

    private fun bindObservable() {
        observableScrollView?.apply {
            observeScrollViewCallbacks(this)
                .debounce(150, TimeUnit.MILLISECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { handleScroll(it) },
                    { loge { "Error: $it, message: ${it.message}" } }
                )
        }
    }

    private fun handleScroll(scroll: Int) {
        if (animating || scrolling) return
        logw { "[$scroll ; ${titleHeight.toInt()}]" }
        when {
            scroll >= titleHeight.toInt() -> {
                if (toolbarTitle!!.alpha < 1.0f) {
                    logw { "Below title. Fade In toolbar" }
                    toolbarTitle!!.fadeIn()
                        .mergeWith(fragmentTitleText!!.fadeOut())
                        .doOnSubscribe { animating = true }
                        .doOnComplete { animating = false }
                        .subscribe()
                }
                if (reveal!!.alpha < 1.0f) {
                    logw { "Below title. Fade in reveal" }
                    reveal!!.fadeIn(100).subscribe()
                }

                toolbarIcon?.let {
                    if (it.color != colorFrom(R.color.white)) {
                        logw { "Below title. Arrow color -> white" }
                        it.changeColor(colorFrom(R.color.white)).subscribe()
                    }
                }
            }

            scroll < titleHeight.toInt() -> {
                if (lastScrollState == ScrollState.UP) {
                    if (!animating && !scrolling) {
                        logw { "Above title. UP. Expand toolbar" }
                        observableScrollView!!
                            .smoothScrollVerticallyTo(titleHeight.toInt())
                            .mergeWith(reveal!!.fadeIn(100))
                            .subscribe()
                    }

                    if (toolbarTitle!!.alpha < 1.0f) {
                        logw { "Above title. UP. Fade In toolbar" }
                        toolbarTitle!!.fadeIn()
                            .mergeWith(fragmentTitleText!!.fadeOut())
                            .doOnSubscribe { animating = true }
                            .doOnComplete { animating = false }
                            .subscribe()
                    }

                    toolbarIcon?.let {
                        if (it.color != colorFrom(R.color.white)) {
                            logw { "Above title. UP. Arrow color -> white" }
                            it.changeColor(colorFrom(R.color.white))
                                .subscribe()
                        }
                    }
                } else {
                    if (!animating && !scrolling) {
                        logw { "Above title. DOWN. Close toolbar" }
                        observableScrollView!!
                            .smoothScrollVerticallyTo(0)
                            .mergeWith(reveal!!.fadeOut(100))
                            .subscribe()
                    }

                    if (toolbarTitle!!.alpha > 0.0f) {
                        logw { "Above title. DOWN. Fade out toolbar" }
                        toolbarTitle!!.fadeOut()
                            .mergeWith(fragmentTitleText!!.fadeIn())
                            .doOnSubscribe { animating = true }
                            .doOnComplete { animating = false }
                            .subscribe()
                    }

                    toolbarIcon?.let {
                        if (it.color != colorFrom(R.color.dark)) {
                            logw { "Above title. DOWN. Arrow color -> dark" }
                            it.changeColor(colorFrom(R.color.dark)).subscribe()
                        }
                    }
                }
            }
        }
    }

    private fun observeScrollViewCallbacks(observableScrollView: ObservableScrollView): Observable<Int> {
        return Observable.create { emitter ->
            val listener = object : ObservableScrollViewCallbacks {

                override fun onUpOrCancelMotionEvent(scrollState: ScrollState?) {
                    scrollState?.let { lastScrollState = it }
                    scrolling = false
                }

                override fun onDownMotionEvent() {
                    scrolling = true
                }

                override fun onScrollChanged(
                    scrollY: Int,
                    firstScroll: Boolean,
                    dragging: Boolean
                ) {
                    if (animating) return

                    emitter.onNext(scrollY)
                    // works when dragging without closer
                    val scrollYF = scrollY.toFloat()
                    if (scrollY in 0..titleHeight.toInt()) {
                        reveal?.alpha = (scrollYF / titleHeight)
                        toolbarTitle?.alpha = (scrollYF / titleHeight)
                        fragmentTitleText?.alpha = 1.0f - (scrollYF / titleHeight) * 2
                    } else {
                        toolbarTitle?.alpha = 1.0f
                        reveal?.alpha = 1.0f
                        fragmentTitleText?.alpha = 0.0f
                    }
                }
            }
            observableScrollView.setScrollViewCallbacks(listener)
        }
    }
}