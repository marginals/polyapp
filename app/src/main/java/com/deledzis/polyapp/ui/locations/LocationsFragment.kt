package com.deledzis.polyapp.ui.locations

import androidx.annotation.LayoutRes
import com.deledzis.polyapp.R
import com.deledzis.polyapp.classes.extensions.logw
import com.deledzis.polyapp.databinding.FragmentLocationsBinding
import com.deledzis.polyapp.ui.base.BaseFragmentWithObservable
import com.deledzis.polyapp.ui.base.getAndroidViewModel

class LocationsFragment : BaseFragmentWithObservable<LocationsViewModel, FragmentLocationsBinding>() {

    @LayoutRes
    override val layoutId: Int = R.layout.fragment_locations

    override fun createViewModel(): LocationsViewModel = getAndroidViewModel()

    override fun onBindData() {
        super.onBindData()
        dataBinding.viewModel = viewModel

        arguments?.let {
            if (it.containsKey("location_id")) {
                logw { "contains Location ID: ${it.getInt("location_id")}" }
                activity.showBackButton()
            }
        }
    }

    override fun onInjectDependencies() {
        commonInjector.inject(viewModel)
    }
}
