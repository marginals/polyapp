package com.deledzis.polyapp.ui.institutes

import androidx.annotation.LayoutRes
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.deledzis.polyapp.R
import com.deledzis.polyapp.classes.extensions.logw
import com.deledzis.polyapp.databinding.FragmentInstitutesBinding
import com.deledzis.polyapp.room.InstituteDatabase
import com.deledzis.polyapp.ui.base.BaseFragmentWithObservable
import com.deledzis.polyapp.ui.base.getAndroidViewModel

class InstitutesFragment : BaseFragmentWithObservable<InstitutesViewModel, FragmentInstitutesBinding>() {

    @LayoutRes
    override val layoutId: Int = R.layout.fragment_institutes

    override fun createViewModel(): InstitutesViewModel = getAndroidViewModel()

    override fun onBindData() {
        super.onBindData()
        dataBinding.viewModel = viewModel
        val db = InstituteDatabase.getInstance(
            requireContext(),
            lifecycleScope
        )
        db.instituteDao().getAll().observe(viewLifecycleOwner, Observer {
            logw { "Collected list of institutes: ${it.size}" }
        })
    }

    override fun onInjectDependencies() {
        commonInjector.inject(viewModel)
    }
}