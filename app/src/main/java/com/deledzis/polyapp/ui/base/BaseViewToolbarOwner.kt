package com.deledzis.polyapp.ui.base

import androidx.navigation.NavController
import androidx.navigation.ui.AppBarConfiguration

interface BaseViewToolbarOwner {
    fun provideNavController(): NavController
    fun provideAppBarConfiguration(): AppBarConfiguration
}