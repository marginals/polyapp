package com.deledzis.polyapp.ui.departments

import androidx.annotation.LayoutRes
import com.deledzis.polyapp.R
import com.deledzis.polyapp.databinding.FragmentDepartmentsBinding
import com.deledzis.polyapp.ui.base.BaseFragmentWithObservable
import com.deledzis.polyapp.ui.base.getAndroidViewModel

class DepartmentsFragment : BaseFragmentWithObservable<DepartmentsViewModel, FragmentDepartmentsBinding>() {

    @LayoutRes
    override val layoutId: Int = R.layout.fragment_departments

    override fun createViewModel(): DepartmentsViewModel = getAndroidViewModel()

    override fun onBindData() {
        super.onBindData()
        dataBinding.viewModel = viewModel
    }

    override fun onInjectDependencies() {
        commonInjector.inject(viewModel)
    }
}