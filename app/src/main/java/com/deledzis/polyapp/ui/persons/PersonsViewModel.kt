package com.deledzis.polyapp.ui.persons

import android.app.Application
import android.os.Bundle
import android.view.View
import com.deledzis.polyapp.R
import com.deledzis.polyapp.ui.base.BaseViewModel

class PersonsViewModel(app: Application) : BaseViewModel(app) {

    fun openPerson(view: View) {
        // TODO: replace with real
        val bundle = Bundle.EMPTY
        navigateOnClick(view, R.id.action_persons_fragment_to_person_fragment, bundle)
    }
}