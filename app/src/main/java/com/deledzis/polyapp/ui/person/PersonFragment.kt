package com.deledzis.polyapp.ui.person

import androidx.annotation.LayoutRes
import com.deledzis.polyapp.R
import com.deledzis.polyapp.databinding.FragmentPersonBinding
import com.deledzis.polyapp.ui.base.BaseFragmentWithObservable
import com.deledzis.polyapp.ui.base.getAndroidViewModel

class PersonFragment : BaseFragmentWithObservable<PersonViewModel, FragmentPersonBinding>() {

    @LayoutRes
    override val layoutId: Int = R.layout.fragment_person

    override fun createViewModel(): PersonViewModel = getAndroidViewModel()

    override fun onBindData() {
        super.onBindData()
        dataBinding.viewModel = viewModel
    }

    override fun onInjectDependencies() {
        commonInjector.inject(viewModel)
    }

    override fun onResume() {
        super.onResume()
        // need this to select structure menu item after returning back from locations fragment
        activity.selectStructureMenuItem()
    }
}