package com.deledzis.polyapp.ui.department

import android.app.Application
import android.os.Bundle
import android.view.View
import com.deledzis.polyapp.ui.base.BaseViewModel

class DepartmentViewModel(app: Application) : BaseViewModel(app) {
    fun openLocations(view: View) {
        // TODO: replace with real
        val bundle = Bundle().apply { putInt("location_id", 7) }
        navigateToLocations(view, bundle)
    }
}