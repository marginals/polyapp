package com.deledzis.polyapp.di.components

import com.deledzis.polyapp.App
import com.deledzis.polyapp.di.modules.AppModule
import com.deledzis.polyapp.ui.base.BaseViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface CommonComponent {
    fun inject(app: App)
    fun inject(vm: BaseViewModel)
}
