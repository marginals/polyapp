package com.deledzis.polyapp.room

import androidx.room.TypeConverter
import com.deledzis.polyapp.classes.Degree
import java.util.*

object Converters {

    @TypeConverter
    @JvmStatic
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    @JvmStatic
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    @JvmStatic
    fun degreeToString(degree: Degree): String = degree.name

    @TypeConverter
    @JvmStatic
    fun stringToDegree(degreeValue: String): Degree = Degree.valueOf(degreeValue)

}