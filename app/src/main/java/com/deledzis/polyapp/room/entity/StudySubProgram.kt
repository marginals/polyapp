package com.deledzis.polyapp.room.entity

import androidx.room.*

@Entity(
    tableName = "study_sub_programs",
    foreignKeys = [ForeignKey(
        entity = StudyProgram::class,
        parentColumns = ["id"],
        childColumns = ["study_program_id"],
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index("study_program_id")]
)
data class StudySubProgram(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @ColumnInfo(name = "study_program_id")
    val studyProgramId: Int,
    val name: String,
    val position: Int,
    @Embedded(prefix = "gov_")
    val govPlaces: EducationForms,
    @Embedded(prefix = "self_")
    val selfPlaces: EducationForms,
    @ColumnInfo(name = "price")
    val price: Int
)