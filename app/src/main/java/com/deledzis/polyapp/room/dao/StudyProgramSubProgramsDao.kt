package com.deledzis.polyapp.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.deledzis.polyapp.room.entity.StudyProgramAndSubPrograms

@Dao
interface StudyProgramSubProgramsDao {

    @Transaction
    @Query("select * from study_programs where id = :id")
    fun getWithSubPrograms(id: Int): LiveData<StudyProgramAndSubPrograms>

}