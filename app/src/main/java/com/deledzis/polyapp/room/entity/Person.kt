package com.deledzis.polyapp.room.entity

import androidx.room.*
import com.deledzis.polyapp.classes.Degree

//TODO: Add Location with Buildings

@Entity(
    tableName = "persons",
    foreignKeys = [
        ForeignKey(
            entity = Institute::class,
            parentColumns = ["id"],
            childColumns = ["institute_id"],
            onDelete = ForeignKey.CASCADE
        ), ForeignKey(
            entity = Department::class,
            parentColumns = ["id"],
            childColumns = ["department_id"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index("institute_id"),
        Index("department_id")
    ]
)
data class Person(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @ColumnInfo(name = "institute_id")
    val instituteId: Int,
    @ColumnInfo(name = "department_id")
    val departmentId: Int,
    @ColumnInfo(name = "full_name")
    val fullName: String,
    val url: String,
    val email: String? = null,
    val phone: String? = null,
    val degree: Degree,
    @ColumnInfo(name = "photo_url")
    val photoUrl: String? = null,
    @ColumnInfo(name = "scopus_url")
    val scopusUrl: String? = null,
    @ColumnInfo(name = "rsci_url")
    val rsciUrl: String? = null,
    val description: String? = null
)