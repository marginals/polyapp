package com.deledzis.polyapp.room.entity

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.room.*

@Entity(tableName = "institutes")
data class Institute(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val name: Int,
    val abbreviation: Int,
    @ColumnInfo(name = "logo_id")
    @DrawableRes val logoId: Int,
    @ColumnInfo(name = "background_id")
    @DrawableRes val backgroundId: Int,
    @ColumnInfo(name = "color_id")
    @ColorRes val colorId: Int,
    @ColumnInfo(name = "color_light_id")
    @ColorRes val colorLightId: Int,
    val url: Int
)

/* Use for getting institutes' study programs */
class InstituteAndAllStudyPrograms(
    @Embedded
    val institute: Institute,

    @Relation(
        entity = StudyProgram::class,
        parentColumn = "id",
        entityColumn = "institute_id"
    )
    val studyPrograms: List<StudyProgram>
)

/* Use for getting institutes' directorate persons */
class InstituteAndDirectoratePersons(
    @Embedded
    val institute: Institute,

    @Relation(
        entity = Person::class,
        parentColumn = "id",
        entityColumn = "institute_id"
    )
    val persons: List<Person>
)

/* Use for getting institutes' departments */
class InstituteAndAllDepartments(
    @Embedded
    val institute: Institute,

    @Relation(
        entity = Department::class,
        parentColumn = "id",
        entityColumn = "institute_id"
    )
    val departments: List<Department>
)