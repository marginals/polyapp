package com.deledzis.polyapp.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.deledzis.polyapp.room.dao.TimelineEventDao
import com.deledzis.polyapp.room.entity.TimelineEvent

@Database(
    entities = [TimelineEvent::class],
    version = 1,
    exportSchema = true
)
@TypeConverters(Converters::class)
abstract class TimelineEventDatabase : RoomDatabase() {

    abstract fun timelineEventsDao(): TimelineEventDao

    companion object {

        private var instance: TimelineEventDatabase? = null

        fun getInstance(context: Context): TimelineEventDatabase {
            if (instance == null) {
                synchronized(TimelineEventDatabase::class) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        TimelineEventDatabase::class.java, "timeline_events.db"
                    )
                        .build()
                }
            }
            return instance!!
        }

        fun destroyInstance() {
            instance = null
        }
    }
}