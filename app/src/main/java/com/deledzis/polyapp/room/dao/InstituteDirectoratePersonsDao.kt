package com.deledzis.polyapp.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.deledzis.polyapp.room.entity.InstituteAndDirectoratePersons

@Dao
interface InstituteDirectoratePersonsDao {

    @Transaction
    @Query("select * from institutes where id = :id")
    fun getWithDirectoratePersons(id: Int): LiveData<InstituteAndDirectoratePersons>

}