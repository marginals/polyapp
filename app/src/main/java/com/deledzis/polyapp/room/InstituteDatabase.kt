package com.deledzis.polyapp.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.deledzis.polyapp.R
import com.deledzis.polyapp.room.dao.InstituteDao
import com.deledzis.polyapp.room.dao.InstituteDepartmentsDao
import com.deledzis.polyapp.room.dao.InstituteDirectoratePersonsDao
import com.deledzis.polyapp.room.dao.InstituteStudyProgramsDao
import com.deledzis.polyapp.room.entity.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(
    entities = [
        Institute::class,
        StudyProgram::class,
        StudySubProgram::class,
        Person::class,
        Department::class
    ],
    version = 2,
    exportSchema = true
)
@TypeConverters(Converters::class)
abstract class InstituteDatabase : RoomDatabase() {

    abstract fun instituteDao(): InstituteDao
    abstract fun instituteStudyProgramsDao(): InstituteStudyProgramsDao
    abstract fun instituteDirectoratePersonsDao(): InstituteDirectoratePersonsDao
    abstract fun instituteDepartmentsDao(): InstituteDepartmentsDao

    private class InstituteDatabaseCallback(private val scope: CoroutineScope) :
        RoomDatabase.Callback() {

        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            instance?.let { database ->
                scope.launch(Dispatchers.IO) {
                    populateDatabase(database.instituteDao())
                }
            }
        }

        suspend fun populateDatabase(instituteDao: InstituteDao) {
            instituteDao.deleteAll()

            val institute = Institute(
                id = 0,
                name = R.string.institute_icst,
                abbreviation = R.string.abbreviation_icst,
                logoId = 0,
                backgroundId = 0,
                colorId = R.color.icst,
                colorLightId = R.color.icst_light,
                url = R.string.icst_url
            )
            instituteDao.insert(institute)
        }
    }

    companion object {

        @Volatile
        private var instance: InstituteDatabase? = null

        fun getInstance(
            context: Context,
            scope: CoroutineScope
        ): InstituteDatabase {
            if (instance == null) {
                synchronized(InstituteDatabase::class) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        InstituteDatabase::class.java, "institutes.db"
                    ).fallbackToDestructiveMigration()
                        .addCallback(InstituteDatabaseCallback(scope))
                        .build()
                }
            }
            return instance!!
        }

        fun destroyInstance() {
            instance = null
        }
    }
}