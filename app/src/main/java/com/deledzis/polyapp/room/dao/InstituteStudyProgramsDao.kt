package com.deledzis.polyapp.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.deledzis.polyapp.room.entity.InstituteAndAllStudyPrograms

@Dao
interface InstituteStudyProgramsDao {

    @Transaction
    @Query("select * from institutes where id = :id")
    fun getWithStudyPrograms(id: Int): LiveData<InstituteAndAllStudyPrograms>

}