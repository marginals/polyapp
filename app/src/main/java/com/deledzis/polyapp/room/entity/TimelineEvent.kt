package com.deledzis.polyapp.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "timeline_events")
data class TimelineEvent(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @ColumnInfo(name = "education_form")
    val educationForm: String,
    @ColumnInfo(name = "funding_form")
    val fundingForm: String,
    val description: String,
    val date: Date,
    @ColumnInfo(name = "is_selected")
    var isSelected: Boolean = false
)