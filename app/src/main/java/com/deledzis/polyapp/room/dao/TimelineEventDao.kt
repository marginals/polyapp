package com.deledzis.polyapp.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.deledzis.polyapp.room.entity.TimelineEvent
import java.util.*

@Dao
interface TimelineEventDao {

    @Query("select * from timeline_events")
    fun getAll(): LiveData<List<TimelineEvent>>

    @Query("select * from timeline_events where date like :date limit 1")
    fun getByDate(date: Date): LiveData<TimelineEvent>

    @Insert
    suspend fun insert(timelineEvent: TimelineEvent)

    @Insert
    suspend fun insert(vararg timelineEvent: TimelineEvent)

    @Delete
    suspend fun delete(timelineEvent: TimelineEvent)

    @Delete
    suspend fun delete(vararg timelineEvent: TimelineEvent)

    @Query("delete from timeline_events")
    suspend fun deleteAll()

    @Update
    suspend fun update(timelineEvent: TimelineEvent)

    @Update
    suspend fun update(vararg timelineEvent: TimelineEvent)
}