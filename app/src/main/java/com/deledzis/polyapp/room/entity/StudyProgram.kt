package com.deledzis.polyapp.room.entity

import androidx.room.*

@Entity(
    tableName = "study_programs",
    foreignKeys = [ForeignKey(
        entity = Institute::class,
        parentColumns = ["id"],
        childColumns = ["institute_id"],
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index("institute_id")]
)
data class StudyProgram(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @ColumnInfo(name = "institute_id")
    val instituteId: Int,
    val code: String,
    @ColumnInfo(name = "field_of_study")
    val fieldOfStudy: String,
    val name: String,
    @Embedded(prefix = "gov_")
    val govPlaces: EducationForms? = null,
    @Embedded(prefix = "self_")
    val selfPlaces: EducationForms? = null,
    @ColumnInfo(name = "price")
    val price: Int? = null
)

/* Use for getting institutes' study programs */
class StudyProgramAndSubPrograms(
    @Embedded
    val studyProgram: StudyProgram,

    @Relation(
        entity = StudySubProgram::class,
        parentColumn = "id",
        entityColumn = "study_program_id"
    )
    val studySubPrograms: List<StudySubProgram>
)
