package com.deledzis.polyapp.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.deledzis.polyapp.room.dao.StudyProgramSubProgramsDao
import com.deledzis.polyapp.room.dao.StudyProgramsDao
import com.deledzis.polyapp.room.entity.Institute
import com.deledzis.polyapp.room.entity.StudyProgram
import com.deledzis.polyapp.room.entity.StudySubProgram

@Database(
    entities = [
        Institute::class,
        StudyProgram::class,
        StudySubProgram::class
    ],
    version = 1,
    exportSchema = true
)
@TypeConverters(Converters::class)
abstract class StudyProgramsDatabase : RoomDatabase() {

    abstract fun studyProgramsDao(): StudyProgramsDao
    abstract fun studyProgramSubProgramsDao(): StudyProgramSubProgramsDao

    companion object {

        @Volatile
        private var instance: StudyProgramsDatabase? = null

        fun getInstance(context: Context): StudyProgramsDatabase {
            if (instance == null) {
                synchronized(InstituteDatabase::class) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        StudyProgramsDatabase::class.java, "study_programs.db"
                    ).build()
                }
            }
            return instance!!
        }

        fun destroyInstance() {
            instance = null
        }
    }
}