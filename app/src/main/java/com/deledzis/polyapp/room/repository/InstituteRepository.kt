package com.deledzis.polyapp.room.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.deledzis.polyapp.room.dao.InstituteDao
import com.deledzis.polyapp.room.entity.Institute

class InstituteRepository(private val instituteDao: InstituteDao) {

    val institutes: LiveData<List<Institute>> = instituteDao.getAll()

    fun getById(id: Int) = instituteDao.get(id)

    @WorkerThread
    suspend fun insert(institute: Institute) {
        instituteDao.insert(institute)
    }

    @WorkerThread
    suspend fun insert(vararg institute: Institute) {
        instituteDao.insert(*institute)
    }

    @WorkerThread
    suspend fun delete(institute: Institute) {
        instituteDao.delete(institute)
    }

    @WorkerThread
    suspend fun delete(vararg institute: Institute) {
        instituteDao.delete(*institute)
    }

    @WorkerThread
    suspend fun deleteAll() {
        instituteDao.deleteAll()
    }

    @WorkerThread
    suspend fun update(institute: Institute) {
        instituteDao.update(institute)
    }

    @WorkerThread
    suspend fun update(vararg institute: Institute) {
        instituteDao.update(*institute)
    }
}