package com.deledzis.polyapp.room.entity

import androidx.room.*

@Entity(
    tableName = "departments",
    foreignKeys = [ForeignKey(
        entity = Institute::class,
        parentColumns = ["id"],
        childColumns = ["institute_id"],
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index("institute_id")]
)
class Department(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @ColumnInfo(name = "institute_id")
    val instituteId: Int,
    val title: String,
    val url: String,
    val email: String? = null,
    val phone: String? = null,
    val logoUrl: String? = null,
    val info: String? = null,
    val descriptionMdFileUrl: String? = null
)

// TODO: Add sub departments
// TODO: Add directorate persons