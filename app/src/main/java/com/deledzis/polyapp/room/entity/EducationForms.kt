package com.deledzis.polyapp.room.entity

import androidx.room.ColumnInfo

class EducationForms(
    @ColumnInfo(name = "ft_places") val fullTimePlaces: Int = 0,
    @ColumnInfo(name = "ex_places") val extramuralPlaces: Int = 0,
    @ColumnInfo(name = "pt_places") val partTimePlaces: Int = 0
)