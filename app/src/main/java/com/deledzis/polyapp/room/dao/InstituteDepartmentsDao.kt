package com.deledzis.polyapp.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.deledzis.polyapp.room.entity.InstituteAndAllDepartments

@Dao
interface InstituteDepartmentsDao {

    @Transaction
    @Query("select * from institutes where id = :id")
    fun getWithDepartments(id: Int): LiveData<InstituteAndAllDepartments>

}