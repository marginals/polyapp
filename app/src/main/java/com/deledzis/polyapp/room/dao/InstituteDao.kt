package com.deledzis.polyapp.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.deledzis.polyapp.room.entity.Institute

@Dao
interface InstituteDao {

    @Query("select * from institutes")
    fun getAll(): LiveData<List<Institute>>

    @Query("select * from institutes where id = :id limit 1")
    fun get(id: Int): LiveData<Institute>

    @Insert
    suspend fun insert(institute: Institute)

    @Insert
    suspend fun insert(vararg institute: Institute)

    @Delete
    suspend fun delete(institute: Institute)

    @Delete
    suspend fun delete(vararg institute: Institute)

    @Query("delete from institutes")
    suspend fun deleteAll()

    @Update
    suspend fun update(institute: Institute)

    @Update
    suspend fun update(vararg institute: Institute)
}