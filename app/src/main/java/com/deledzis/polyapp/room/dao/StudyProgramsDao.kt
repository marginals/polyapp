package com.deledzis.polyapp.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.deledzis.polyapp.room.entity.StudyProgram

@Dao
interface StudyProgramsDao {

    @Query("select * from study_programs")
    fun getAll(): LiveData<List<StudyProgram>>

    @Query("select * from study_programs where id = :id limit 1")
    fun get(id: Int): LiveData<StudyProgram>

    @Insert
    suspend fun insert(studyProgram: StudyProgram)

    @Insert
    suspend fun insert(vararg studyProgram: StudyProgram)

    @Delete
    suspend fun delete(studyProgram: StudyProgram)

    @Delete
    suspend fun delete(vararg studyProgram: StudyProgram)

    @Query("delete from study_programs")
    suspend fun deleteAll()

    @Update
    suspend fun update(studyProgram: StudyProgram)

    @Update
    suspend fun update(vararg studyProgram: StudyProgram)
}