package com.deledzis.polyapp

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.deledzis.polyapp.di.components.CommonComponent
import com.deledzis.polyapp.di.components.DaggerCommonComponent
import com.deledzis.polyapp.di.modules.AppModule

class App : Application() {

    private lateinit var _commonInjector: CommonComponent
    val commonInjector: CommonComponent
        get() = _commonInjector

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        _commonInjector = DaggerCommonComponent
            .builder()
            .appModule(AppModule(this))
            .build()

        _commonInjector.inject(this)
    }
}